'use strict';

const path = require('path');

module.exports = {
    entry: './src/index.ts',
    devtool: 'eval-source-map',
    mode:'development',
    output: {
        publicPath: 'public',
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    resolve:{
        extensions: ['.ts','.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                include: [path.resolve(__dirname, 'src')]
            }
        ]
    }
}