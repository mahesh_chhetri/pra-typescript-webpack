/**
 * Some small points are to be noted
 */

 //Note: 
const obj = { width: 10, height: 15 };
// const area = obj.width * obj.heigth; This doesn't warn us 
// anything in javascript returns the NAN only
// Whereas heigth (spelling mistake here)


// Another exampla of without warning in javascript
// console.log(4 / []);
// in typescript - The right-hand side of an arithmetic operation 
// must be of type 'any', 'number', 'bigint' or an enum type.


/**
 * Classes
 */


interface User {
    name: string;
    id: number;
  }
  
  class UserAccount{
    // name:string;
    // id:number;  
  
    constructor() {
      // this.name =  "dfd";
      // this.id = 46456;
    }
  }
  

  // const user: UserAccount = new UserAccount();