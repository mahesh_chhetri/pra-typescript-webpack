/**
 * Here is the typescript basic types documentation
 * 
 * Note: Only those things are written here which are complex to uinderstand
 */

//================>>Topic: Number <<================================
// decimal, hex, binary, octal, big integer
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
// let big: bigint = 100n; // It doesn't support in less then ES2020



//================>>Topic: Array <<================================

// (1) followed by  []
let list: number[] = [1, 2, 3];
let list2: string[] = ['1', '2', '3'];
// (2) Generic array followed by Array<elemType>
let list3: Array<number> = [1, 2, 3];
let list4: Array<string> = ['1', '2', '3'];


//================>>Topic: Tuple <<================================
// It gives you the way to declare the mix type of array values
// example: (1)
let x: [string, number];
x = ["hello", 10]; // OK
// x = [10, "hello"]; // Error

//Example:  (2)
let x2: [number, string];
x2 = [10, "hello"]; // OK
// x2 = ["hello",34]; // Error

// Example: (3)
let x3: [number, string, number, string, string];
x3 = [10, "hello", 45, 'sdd', 'sd']; // OK


//==========================>>Topic: Enum  <<======================
// This gives you the way to compare values in string mode
// Normally emum keys gives you the index number starting from 0 
// but we can override them, see following example
enum Colors {
    red = 'red',
    green = "green",
    yellow = 'yellow',
    blue = 'blue',
    magenta = 'magenta',
    cyan = 'cyan',
    white = 'white',
    black = 'black'
}
let myColor: Colors = Colors.green;
// console.log("My color", myColor);
// Enum  with function
function testColor(color: Colors) {
    console.log("test color", color);
}
// testColor(Colors.red);

//================>>Topic: unknown <<================================
// Generally, if we don't know about comming value 
// from user then we can use unknown type
// Anytype of value can be assigned to it
let notSure: unknown = 4;
notSure = "maybe a string instead";
notSure = false;
notSure = ['cat', 45656, 'dog'];
// It changes its type dynamically, see example below
if (notSure === true) { // TypeScript knows that maybe is a boolean now
    const aBoolean: boolean = notSure;
    // So, it cannot be a string
    // const aString: string = notSure; //  Type 'boolean' is not assignable to type 'string'.
}
if (typeof notSure === "string") {
    // TypeScript knows that maybe is a string
    const aString: string = notSure;
    // So, it cannot be a boolean
    //const aBoolean: boolean = notSure; //Type 'string' is not assignable to type 'boolean'
}

//================>>Topic: any <<================================
// unlike unknown, variables of type any allow you to access arbitrary properties,
let looselyTyped: any = 4;
// OK, ifItExists might exist at runtime
// looselyTyped.ifItExists();
// OK, toFixed exists (but the compiler doesn't check)
looselyTyped.toFixed();

let strictlyTyped: unknown = 4;
//strictlyTyped.toFixed(); // Object is of type 'unknown'.

//================>>Topic: void <<================================
// Used to say a function doesn't return any value or type
// we can use string, number, array, boolean etc to set the return type

function warnUser(): void {
    console.log("This is my warning message");
    return undefined; // undefined also assignable to void
}

let unusable: void = undefined;

//================>>Topic: undefined and null <<================================
// It is a simple types and it can also occur in other types too. see the documentation


//================>>Topic: Never <<================================
// This type is use to function which never returns 
// see exmaples below

// Function returning never must not have a reachable end point
function error(message: string): never {
    throw new Error(message);
}

// Inferred return type is never
function fail() {
    return error("Something failed");
}

// Function returning never must not have a reachable end point
function infiniteLoop(): never {
    while (true) { }
}

//================>>Topic: Object <<================================
// It is the non-premitive type i.e not number, string, boolean, symbol, null, or undefined
// see an example 

function myFunction(data: object | number):void{

}
// when calling 
myFunction({ name: 'Adrin' })       // OK
myFunction(4545)                    // OK
myFunction(() => { })               // OK
myFunction([])                      // OK
myFunction([1, 2, 3, 4, 4])         // OK
//myFunction("foo")                 // you can not  pass string
//myFunction(null)                  // you can not pass null
myFunction(3434)                    // you can not pass null



//================>>Topic: Type assertions <<================================
// A type assertion is like a type cast in other languages, 
// but performs no special checking or restructuring of data
// Type assertions have two forms
// (1)

let someValue: any = "this is a string";
let strLength: number = (someValue as string).length;

// (2)
let someValue2: any = "this is a string";
let strLength2: number = (<string>someValue).length;

// Example - 01
let someValue3: any = 3543543;
let strLength3: number = (<string>someValue).length; // here we have converted into string
// Example -02
let someValue4: any = 2324;
let strLength4: number = (someValue4 as string).length; // here we have converted into string(2); // here we have converted into string
