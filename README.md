# Practice typescript with webpack

### Project setup 
1. `npm install typescript -g` - We are installing typescript globally for now to create the typescript config file.
1. Once typescript installed globally run the `tsc --init` - It will generates the default typescript config file
1. now, change couple of things in config file 
> `"module": "commonjs"` to `"module": "es2015"` - we are gonna writing code in es2015

1. Now, install following packages 
`npm install -D webpack webpack-cli ts-loader typescript`

we have installed typescript locally to access it locally

1. Create `src` and `public` folder for source and destination respectevly.
Whereas, `src` for all source typescript files and `public` folder will be for public access.

1. Inside `public` folder create `index.html` file
1. create `index.ts` file inside `src` folder

Finally, we have finished setting up the typescript confguration and now we need to setup `babel`. 

To do so, create a file and name it `webpack.config.js` and paste the following script 
```javascript
'use strict';
const path = require('path');
module.exports = {
    entry: './src/index.ts',
    mode:'development',
    output: {
        publicPath: 'public',
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    resolve:{
        extensions: ['.ts','.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                include: [path.resolve(__dirname, 'src')]
            }
        ]
    }
}
```

1. In `package.json` file add the following script
```javascript
"scripts": {
    "build": "webpack"
  }
```

### setting up webpack live reload
`npm install webpack-dev-server`

Now,update the package.json file with ` "serve": "webpack-dev-server",`
now try to run `npm run serve` - It should run run the server


### Enabling sourcemap for debugging
1. go to `tsconfig.json` and enable `sourceMap`
1. now, add the script `devtool: 'eval-source-map'` to webpack.config.js file 

It should help you to debugging