/**
 * Here we have covered maximum topics on interface
 * 
 * It's a way to set the rule for data types and some other formated values
 */

// Out first interface goes here

// Here we are saying whatever the `data` argument contains  is
// object properties but name must be `string`
function testFunction(data: { name: string }): string {
    return `My name is ${data.name}`;
}
let myParameter = { name: 'Mahesh', age: 13, phone: 4645654645 }
// let myParameter = { name: 6546456, age: 13, phone: 4645654645 } - Name shouldn't be number
testFunction(myParameter);

